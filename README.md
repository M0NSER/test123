W pierwszej koleności na maszynie musi byc zainstalowany docker engine (docker-ce), oraz docker compose.

Zamiast budować całe obrazy za każdym razem i trzymać jakieś dziwne polecenia uruchomieniowe z masą flag można wykonać polecenie (znajdując się w katalogu z docker-compose.yml):
```
docker-compose up -d
```

To utworzy wszystkie niezbędne kontenery z odpowiednimi konfiguracjami, później można spokojnie już działać na samym `docker start/stop/logs/etc.`

Najważniejszą częścią tej konstrukcji jest `nginx-proxy` w załączonych plikach należy jedynie podmienić default_email który bedzie używany do rejestracji SSL-a w serwisach.
> **Ważne:_**  Za każdym razem gdy wystawiona będzie nowa aplikacja, która będzie miała wystawioną zmienną środowiskową `VIRTUAL_HOST` należy przeładować pod z `nginx-proxy`, jeśli po odpaleni ustrony będzie krzyczało błędem o nieprawidłowym certyfikacie dobrze jest też przeładować `nginx-ssl`


Wszystko inne będzie już operowało na bardzo podobnej zasadzie w przypadku zarówno katalogu `database` jak i `example-app` w docker-compose są zdefiniowane 2 kontenery, jeden, który będzie służył jako *"entry point"* dla ruchu z internetu. Natomiast drugi jest już samą aplikacją.

W załączonym `example-app` dodałem dosłowną konfigurację aplikacji.  `api-container` buduje się z obrazu który sam wytworzyłem (*ta część jest analogiczna do tego jak budujemy apki w DOZ*) i jest dostępny tylko wewnątrz VPS-a, aplikacja exponuje dane na porcie 8080, który zmapowałem na 8081, żeby nie blokować portu w ewntualnym użyciu. natomiast `api-website` będzie wystawione po porcie 80 (443 jest obsługiwany via `nginx-proxy` gdybym wystawiał tu szyfrowany port to na zwrotkach z API dostawalibyśmy krzaki).

Ostatni punkt to skierowanie ruchu. W katalogu `/etc/nginx/vhost.d/` należy utworzyć pliczek o nazwie domeny którą skierowaliśmy w DNS-ach na tego VPS-a plus `'_location'`, a także zdefiniowaliśmy ją w którymś kontenerze jako `VIRTUAL_HOST`.

W zawartości tego pliczku możemy dostarczyć specyficzną konfigurację nginx dla tej aplikacji, oraz podajemy proxy_pass, który będzie nazwą kontenera aplikacji, oraz docelowy port na którym nadaje zwrotki.
